$(document).ready( f => {

  const {ipcRenderer, clipboard, remote} = require('electron'); //destructure modules.

  var ClipBoardModel = function() {

      var self = this;
      var history = ko.observableArray();
      var lastText = clipboard.readText();
      this.filterText = ko.observable('');
      this.color1Hex = ko.observable('');
      this.color2Hex = ko.observable('');
      this.backgroundHex = ko.observable('');
      this.textHex = ko.observable('');

      //this is the function that is used when filtering.
      var historyComparator = (item) => {
        var lhs = item.name.toLowerCase();
        var rhs = this.filterText().toLowerCase();
        return (lhs.search(rhs) != -1);
      };

      //contains the filtered list based on our text.
      this.historyFilter = ko.computed( function() {
          ipcRenderer.send('quick-display-text-request', 'filtering..');
        return history().filter(historyComparator);
      });

      //this function handles setting up the history that has been loaded from the main process.
      ipcRenderer.on('saved-history-response', (event,arg) => {
        history(arg);
      });

      //request the history from the main process.
      ipcRenderer.on('request-close', (event,arg) => {
        var colorScheme = {
          background: this.backgroundHex(),
          text: this.textHex(),
          color1: this.color1Hex(),
          color2: this.color2Hex()
        };
        ipcRenderer.send('request-close-response', {history: history(), colorScheme: colorScheme});
      });

      //request the history from the main process.
      ipcRenderer.on('request-color-scheme-response', (event,arg) => {
        this.backgroundHex(arg.background);
        this.textHex(arg.text);
        this.color1Hex(arg.color1);
        this.color2Hex(arg.color2);
      });

      //main process request that the UI update the new clipboard value.
      ipcRenderer.on('update-request', (event,arg) => {
        if (lastText != clipboard.readText()) {
          history.unshift({'name': clipboard.readText()});
          lastText = clipboard.readText();
          ipcRenderer.send('quick-display-text-request', 'copied clipboard text: ' + lastText.substr(0,5) + "...");
        }
      });

      // tell the main process to update the clipboard.
      this.onClick = function(text) {
          clipboard.writeText(text);
          lastText = text;
          ipcRenderer.send('quick-display-text-request', 'selected clipboard text: ' + lastText.substr(0,5) + "...");
      };

      //clears the items in history filter from the history list.
      this.clear = function() {
        if (this.filterText() != '')
          history.removeAll(this.historyFilter());
        else
          history.removeAll();
        this.filterText('');

        ipcRenderer.send('quick-display-text-request', 'cleared text...');
      };

      //init for object.
      var init = (function() {
        ipcRenderer.send('request-saved-history');
        ipcRenderer.send('request-color-scheme');
      })();
  };

  ko.bindingHandlers.stripeEven = {
    update: function(element, valueAccessor, allBindings) {
        // First get the latest data that we're bound to
        var value = valueAccessor();

        // Now manipulate the DOM element
        $(".clipItem:even").css('background-color', ko.unwrap(value)); // Make the element visible
    }
  };

  ko.bindingHandlers.stripeOdd = {
    update: function(element, valueAccessor, allBindings) {
        // First get the latest data that we're bound to
        var value = valueAccessor();
        // Now manipulate the DOM element
        $(".clipItem:odd").css('background-color', ko.unwrap(value)); // Make the element visible
    }
  };

  ko.applyBindings(new ClipBoardModel());

});
