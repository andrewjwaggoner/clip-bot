const {clipboard, app, BrowserWindow, Tray, Menu, MenuItem, ipcMain} = require('electron'); //destructure modules.
const fs = require('fs');

const offlineStoragePath = app.getPath('temp') + '/' + app.getName() +'-storage.txt';
const offlineColorSchemePath = app.getPath('temp') + '/' + app.getName() + '-colorScheme.json';
let _window = null;
let _needsToClose = false;
let _loaded = false;

//load the history from the last execution and push it to the client.
ipcMain.on('request-saved-history', (event,arg) => {
  fs.stat(offlineStoragePath, (err) => {
    if (!err) {
      var history = JSON.parse( fs.readFileSync(offlineStoragePath, 'utf8'));
      _window.webContents.send('saved-history-response', history);
    }
  });
});

ipcMain.on('request-close-response', (event, arg) => {
  fs.writeFile(offlineColorSchemePath, JSON.stringify(arg.colorScheme), function() {
    fs.writeFile(offlineStoragePath, JSON.stringify(arg.history), function() { app.quit();} );
  });
});

ipcMain.on('request-color-scheme', (event,arg) => {
  fs.stat(offlineColorSchemePath, (err) => {
    if (!err) {
      var scheme = JSON.parse( fs.readFileSync(offlineColorSchemePath, 'utf8'));
      _window.webContents.send('request-color-scheme-response', scheme);
    }
  });
})

ipcMain.on('quick-display-text-request', (event,arg) => {
  if(!_loaded) return;

  _window.setTitle(arg);
  setTimeout(function() { _window.setTitle('clip-bot v1.0');}, 1000);
});

///// app events ///////////////
app.on('ready', f=> {
  //BrowserWindow setup
  _window = new BrowserWindow({width: 800, height: 600, icon: __dirname + "/icon.png", webPreferences: {nodeIntegration: true}});
  _window.loadURL(`file://${__dirname}/main-screen/markup.html`)
  _window.on('close', (event, arg) => {
    //request the history to be saved off.
    _window.webContents.send('request-close');
    if (!_needsToClose) event.preventDefault(); //prevent close if we haven't tried yet.
      _needsToClose = true;
  });

  // initializes the clipboard.
  var init = (f => {
    setInterval(function() {
      _window.webContents.send('update-request')
    },250);
  })();


    // initializes the clipboard.
    (f => {setInterval(function() { _loaded = true; },1500);})();
});

app.on('closed', f=> {
  _window = null;
});
