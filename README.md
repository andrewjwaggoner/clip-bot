What it is: Clip-bot is a simple electron program that acts as a longterm clipboard for your OS. It retains your clipboard history when closed, and additionally, has a search feature to filter your clipboard history.
You can start the program by running `npm start`. If you'd like to build the .exe yourself, look into https://andrewjwaggoner@bitbucket.org/andrewjwaggoner/clip-bot.git

![Screenshot from 2017-04-07 15-46-32.png](https://bitbucket.org/repo/AgGqp8d/images/488567859-Screenshot%20from%202017-04-07%2015-46-32.png)

Executables for Windows, Linux, Mac are attached.